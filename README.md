# Stadafint
![mainpage](./src/assets/presentation/MainPage.PNG)
<br>
<i> Loginpage </i>
<br>
<br>
## The project
---
Stadafint is one of our biggest project thus far.
In this project we as a group of four were tasked with making a fontend site in React with TypeScript, as well as a backend logic with help of MERN.

![booking](./src/assets/presentation/booking.PNG)
<i>Customers page</i>

<br>

---

The group were tasked to implement features taught in school and in the end have a fully functional site where the customer could book a cleaning appointment and edit it.
<br>
<br>
To also make the cleaner logic where as the cleaner would be able to see incoming jobs and past appointments.

![cleanerpage](./src/assets/presentation/cleanerpage.PNG)

<i>Cleaners page</i>

---

### Tools used:
In this project we touched upon tools like:

useState, useParams, axios, props, useNavigation, context and more.
